import 'package:flutter/material.dart';


class Plazest extends StatefulWidget {

  @override
  _PlazestState createState() => _PlazestState();
}

class _PlazestState extends State<Plazest> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                _boton("Ubicación Rapida"),
                _boton("Ingrese Ubicación Actual")
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                _boton("Aceptar"),
                
              ],
            ),
             Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                _boton("Atras"),
              ],
            )
          ],
        ),
      ),
    );
  }

}
Widget _boton(String texto) {
  return RaisedButton( 
      child:Text(texto, style: TextStyle(color: Colors.white),),
      color: Colors.red,
      elevation: 20,
      padding: EdgeInsets.all(50),
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
          side: BorderSide(color: Colors.deepOrange),
      ),
      onPressed: ()=>{
      
        print("BOTON: " +texto)
      },
  );
}