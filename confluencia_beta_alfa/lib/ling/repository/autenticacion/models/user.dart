import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

class User extends Equatable {
  final String? email;
  final String id; 
  final String? name;
  final String? photo;

  const User({
    required this.email,
    required this.id,
    required this.name,
    required this.photo
  }) : assert(email != null),
  assert(id != null);
  



  static const empty = User(email: '', name: '', photo: '', id: '');


  @override
  List<Object> get props => [id];
}