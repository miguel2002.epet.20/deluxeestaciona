import 'package:flutter/material.dart';
import 'main.dart';

class Patentes extends StatefulWidget {

  @override
  _PatentesState createState() => _PatentesState();
}

class _PatentesState extends State<Patentes> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
         children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                _boton("Agregar Patentes"),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                _boton("Aceptar"),
                
              ],
            ),
             Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                _boton("Atras"), 
              ],
             
            )
          ],
        ),
      ),
    );
  }

}
Widget _boton(String texto) {
  return RaisedButton( 
      child:Text(texto, style: TextStyle(color: Colors.white),),
      color: Colors.pink,
      elevation: 20,
      padding: EdgeInsets.all(50),
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
          side: BorderSide(color: Colors.deepOrange),
      ),
      onPressed: ()=>{
      
        print("BOTON: " +texto)
      },
  );
}