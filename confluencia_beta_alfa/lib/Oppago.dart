import 'package:flutter/material.dart';

class Oppago extends StatefulWidget {

  @override
  _OppagoState createState() => _OppagoState();
}

class _OppagoState extends State<Oppago> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
         children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                _boton("Credito"),
                _boton("Efectivo")
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                _boton("Aceptar"),
                
              ],
            ),
             Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                _boton("Atras"),
              ],
            )
          ],
        ),
      ),
    );
  }

}
Widget _boton(String texto) {
  return RaisedButton( 
      child:Text(texto, style: TextStyle(color: Colors.white),),
      color: Colors.red,
      elevation: 20,
      padding: EdgeInsets.all(50),
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
          side: BorderSide(color: Colors.deepOrange),
      ),
      onPressed: ()=>{
      
        print("BOTON: " +texto)
      },
  );
}