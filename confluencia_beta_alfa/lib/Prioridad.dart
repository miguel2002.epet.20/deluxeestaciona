import 'package:flutter/material.dart';


class Prioridad extends StatefulWidget {

  @override
  _PrioridadState createState() => _PrioridadState();
}

class _PrioridadState extends State<Prioridad> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                _boton("Boton 1"),
                _boton("Boton 2")
              ],
            ),
          ],
        ),
      ),
    );
  }

}
Widget _boton(String texto) {
  return RaisedButton( 
      child:Text(texto, style: TextStyle(color: Colors.white),),
      color: Colors.red,
      elevation: 20,
      padding: EdgeInsets.all(50),
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
          side: BorderSide(color: Colors.deepOrange),
      ),
      onPressed: ()=>{
      
        print("BOTON: " +texto)
      },
  );
}