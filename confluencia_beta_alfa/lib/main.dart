import 'package:flutter/material.dart';
import 'Patentes.dart';

void main() async {
  var firebase;
  await firebase.initialize.App();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Estimations confluencia',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      initialRoute: '/',
      routes: {
        '/': (context) => MyApp(),
        '/Patentes': (context) => Patentes(),
      },
    );
  }
}


