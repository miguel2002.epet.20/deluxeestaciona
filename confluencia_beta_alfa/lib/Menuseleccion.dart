import 'package:flutter/material.dart';

class MenuSeleccion extends StatefulWidget {
  @override
  _MenuSeleccionState createState() => _MenuSeleccionState();
}

class _MenuSeleccionState extends State<MenuSeleccion> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                _boton("Tiempo"),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [_boton("Plaza Estacionamiento")],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                _boton("Estacionamientos"),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [_boton("Estacionamiento Prioridad")],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                _boton("Atras"),
              ],
            )
          ],
        ),
      ),
    );
  }
}

Widget _boton(String texto) {
  // ignore: deprecated_member_use
  return RaisedButton(
    child: Text(
      texto,
      style: TextStyle(color: Colors.white),
    ),
    color: Colors.red,
    elevation: 20,
    padding: EdgeInsets.all(50),
    shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.circular(10.0),
      side: const BorderSide(color: Colors.deepOrange),
    ),
    onPressed: () => {print("BOTON: " + texto)},
  );
}
