// ignore_for_file: file_names

import 'package:flutter/material.dart';
import 'package:untitled/Menuseleccion.dart';
import 'package:untitled/login_final/components/rounded_button.dart';
import 'package:untitled/login_final/constants.dart';
class Estilib extends StatefulWidget {

@override
 _EstilibState createState() => _EstilibState();
}

class _EstilibState extends State<Estilib> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
         children: [
              RoundedButton(
              text: "Atras",
              color: kPrimaryLightColor,
              textColor: Colors.black,
              press: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) {
                      return MenuSeleccion();
                    },
                  ),
                );
              },
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                _boton("Aceptar"),
                
              ],
            ),
             Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                _boton("Atras"),
              ],
            )
          ],
        ),
      ),
    );
  }

}
Widget _boton(String texto) {
  return RaisedButton( 
      child:Text(texto, style: TextStyle(color: Colors.white),),
      color: Colors.pink,
      elevation: 20,
      padding: EdgeInsets.all(50),
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
          side: BorderSide(color: Colors.deepOrange),
      ),
      onPressed: ()=>{
      
        print("BOTON: " +texto)
      },
  );
}